# 🌎 [Planet KDE](https://planet.kde.org)

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_planet-kde-org)](https://binary-factory.kde.org/job/Website_planet-kde-org/)

Planet KDE is a web feed aggregator that collects blog posts from people who contribute to KDE.

If you are a KDE contributor you can have your blog on Planet KDE. Blog content should be mostly
KDE themed and not liable to offend. If you have a general blog you may want to set up a tag and
subscribe the feed for that tag only to Planet KDE.

## Adding your feed

Anyone in the KDE group on invent.kde.org can add new feeds to this repository.

If you want to get your feed added, we prefer Merge Requests via [invent.kde.org](https://invent.kde.org/websites/planet-kde-org).

- Fork this repository
- Edit [planet.ini](https://invent.kde.org/websites/planet-kde-org/tree/master/planet.ini) and add:

```ini
[id]          # replace id with your feed's unique identifier (a-z0-9-_) (eg. kde-dot)
  feed      = # url to your rss/atom feed                                (eg. https://dot.kde.org/rss.xml)
  link      = # link to the main page of your website                    (eg. https://dot.kde.org)
  location  = # language code of the feed, check config.yaml             (eg. en)
  title     = # (optional) title of your feed                            (eg. KDE Dot)
              #   will be used as the `author` field of a post
              #   if this is not set, the title in your feed will be used
  avatar    = # (optional) filename or url of your avatar                (eg. kde.png)
  author    = # (optional) space-separated list of flairs of the author, currently supports:
              #   irc:freenode_nickname     (eg. irc:myircnickname)
              #   matrix:@username:url      (eg. matrix:@dot:kde.org)
              #   telegram:username         (eg. telegram:dotkde)
              #   sok
              #   gsoc
```

- Upload your avatar to [static/hackergotchi directory](https://invent.kde.org/websites/planet-kde-org/tree/master/static/hackergotchi)
- Send a Pull Request

If you do not have a Git account, [file a bug in Bugzilla](https://bugs.kde.org/enter_bug.cgi?product=planet%20kde) listing your name, Git account (if you have one), IRC nick (if you have one), RSS or Atom feed and what you do in KDE. Attach a photo of your face for hackergotchi.

## Planet KDE Guidelines

Planet KDE is one of the public faces of the KDE project and is read by millions of users and potential
contributors. The content aggregated at Planet KDE is the opinions of its authors, but the sum of that
content gives an impression of the project. Please keep in mind the following guidelines for your blog
content and read the [KDE Code of Conduct](https://kde.org/code-of-conduct/). The KDE project
reserves the right to remove an inappropriate blog from the Planet. If that happens multiple times, the
Community Working Group can be asked to consider what needs to happen to get your blog aggregated again.

If you are unsure or have queries about what is appropriate contact the KDE Community Working Group.

### Blogs should be KDE themed

The majority of content in your blog should be about KDE and your work on KDE. Blog posts about personal
subjects are also encouraged since Planet KDE is a chance to learn more about the developers behind KDE.
However blog feeds should not be entirely personal, if in doubt set up a tag for Planet KDE and subscribe
the feed from that tag so you can control what gets posted.

### Posts should be constructive

Posts can be positive and promote KDE, they can be constructive and lay out issues which need to be
addressed, but blog feeds should not contain useless, destructive and negative material. Constructive
criticism is welcome and the occasional rant is understandable, but a feed where every post is critical
and negative is unsuitable. This helps to keep KDE overall a happy project.

### You must be a KDE contributor

Only have your blog on Planet KDE if you actively contribute to KDE, for example through code, user
support, documentation etc.

### It must be a personal blog, or in a blog class

Planet KDE is a collection of blogs from KDE contributors.

### Do not inflame

KDE covers a wide variety of people and cultures. Profanities, prejudice, lewd comments and content
likely to offend are to be avoided. Do not make personal attacks or attacks against other projects on
your blog.

For further guidance on good practice see the [KDE Code of Conduct](https://kde.org/code-of-conduct/).

## Development environment
As a (Hu)Go module, this website requires both Hugo and Go to work. Ruby is also required as a Ruby script is used to retrieve posts.

Read about the shared theme at [kde-hugo wiki](https://invent.kde.org/websites/aether-sass/-/wikis/Hugo).

To run this website locally, after having the shared theme set up, use the following commands:

```sh
bundler config set path '_vendor'
bundler install
bundler exec pluto update planet.ini
bundler exec ruby scripts/hugo-planet.rb
# (optional) to get translated strings and update the language feed selection
export PACKAGE=websites-planet-kde-org
hugoi18n fetch pos # fetch translations into folder "pos"
hugoi18n compile pos # compile translations in folder "pos"
python3 scripts/custom_generation.py # generate target files from translations
# run the development server
hugo server
```

and visit [http://localhost:1313](http://localhost:1313).

_The Dockerfile is out of date after the website is ported to Hugo. Help us update it!_

If you don't have Ruby on your system or simply prefer a docker / docker-compose workflow:

```sh
docker-compose up --build --detach
```

and when you're done:

```sh
docker-compose down
```

## I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).
